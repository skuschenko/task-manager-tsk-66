package com.tsc.skuschenko.tm.repository.model;

import com.tsc.skuschenko.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProjectRepository extends AbstractRepository<Project> {

    @Modifying
    @Query("DELETE FROM Project e")
    void clearAllProjects();

    @Query("SELECT e FROM Project e")
    @Nullable List<Project> findAll();

    @Query("SELECT e FROM Project e WHERE e.id = :id")
    @Nullable Project findOneById(@Param("id") @NotNull String id);

    @Query("SELECT e FROM Project e")
    @Nullable List<Project> findOneByIndex();

    @Query("SELECT e FROM Project e WHERE e.name = :name")
    @Nullable Project findOneByName(@Param("name") @NotNull String name);

    @Query("SELECT e FROM Project e WHERE e.id = :id")
    @Nullable Project findProjectById(@Param("id") @NotNull String id);

    @Modifying
    @Query("DELETE FROM Project e WHERE e.id = :projectId")
    void removeOneById(@Param("projectId") @NotNull String projectId);

}