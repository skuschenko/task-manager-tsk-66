package com.tsc.skuschenko.tm.repository.dto;

import com.tsc.skuschenko.tm.dto.TaskDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TaskDTORepository extends AbstractDTORepository<TaskDTO> {

    @Modifying
    @Query("DELETE FROM TaskDTO e WHERE e.userId = :userId")
    void clear(@NotNull String userId);

    @Modifying
    @Query("DELETE FROM TaskDTO e")
    void clearAllTasks();

    @Query(
            "SELECT e FROM TaskDTO e WHERE e.userId = :userId and " +
                    "e.projectId = :projectId"
    )
    @Nullable List<TaskDTO> findAllTaskByProjectId(
            @Param("userId") @NotNull String userId,
            @Param("projectId") @NotNull String projectId
    );

    @Query("SELECT e FROM TaskDTO e")
    @Nullable List<TaskDTO> findAllTaskDTO();

    @Query("SELECT e FROM TaskDTO e WHERE e.userId = :userId")
    @Nullable List<TaskDTO> findAllWithUserId(
            @Param("userId") @NotNull String userId
    );

    @Query(
            "SELECT e FROM TaskDTO e WHERE e.userId = :userId and " +
                    "e.id = :id"
    )
    @Nullable TaskDTO findOneById(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

    @Query("SELECT e FROM TaskDTO e WHERE e.userId = :userId")
    @Nullable List<TaskDTO> findOneByIndex(
            @Param("userId") @NotNull String userId
    );

    @Query(
            "SELECT e FROM TaskDTO e WHERE e.userId = :userId and " +
                    "e.name = :name"
    )
    @Nullable TaskDTO findOneByName(
            @Param("userId") @NotNull String userId,
            @Param("name") @NotNull String name
    );

    @Query("SELECT e FROM TaskDTO e WHERE e.id = :id ")
    @Nullable TaskDTO findTaskById(@Param("id") @NotNull String id);

    @Modifying
    @Query(
            "DELETE FROM TaskDTO e WHERE e.userId = :userId and" +
                    " e.id = :taskId")
    void removeOneById(
            @Param("userId") @NotNull String userId,
            @Param("taskId") @NotNull String taskId
    );

}
