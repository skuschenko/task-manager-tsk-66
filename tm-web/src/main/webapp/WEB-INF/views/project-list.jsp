<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Project List</h1>

<div class = "table table__model">
	<div class = "table__header model__header">
		<div class = "table__header_tr model__header_tr">
			<div class = "table__header_tr__td model__header_tr__td td_1">
				Id
			</div>
			<div class = "table__header_tr__td model__header_tr__td td_2">
				Name
			</div>
			<div class = "table__header_tr__td model__header_tr__td td_3">
				Status
			</div>
			<div class = "table__header_tr__td model__header_tr__td td_4">
				Description
			</div>
			<div class = "table__header_tr__td model__header_tr__td td_5">
				Date Start
			</div>
			<div class = "table__header_tr__td model__header_tr__td td_6">
				Date Finish
			</div>
			<div class = "table__header_tr__td model__header_tr__td td_7">
				Edit
			</div>
			<div class = "table__header_tr__td model__header_tr__td td_8">
				Delete
			</div>
		</div>
	</div>
	<div class = "table__body model__body">
		<c:forEach var="project" items="${projects}">
            <div class = "table__body_tr model__body_tr">
				<div class = "table__body_tr__td model__body_tr__td td_1">
					<c:out value="${project.id}"/>
				</div>
				<div class = "table__body_tr__td model__body_tr__td td_2">
					<c:out value="${project.name}"/>
				</div>
				<div class = "table__body_tr__td model__body_tr__td td_3">
					<c:out value="${project.status.displayName}"/>
				</div>
				<div class = "table__body_tr__td model__body_tr__td td_4">
					<c:out value="${project.description}"/>
				</div>
				<div class = "table__body_tr__td model__body_tr__td td_5">
			    	<fmt:formatDate value="${project.dateStart}" pattern="dd.MM.yyyy"/>
				</div>
				<div class = "table__body_tr__td model__body_tr__td td_6">
				    <fmt:formatDate value="${project.dateFinish}" pattern="dd.MM.yyyy"/>
				</div>
				<div class = "table__body_tr__td model__body_tr__td td_7">
					<a href="/project/edit/${project.id}">Edit</a>
				</div>
				<div class = "table__body_tr__td model__body_tr__td td_8">
					<a href="/project/delete/${project.id}">Delete</a>
				</div>
				</div>
			</c:forEach>
		</div>
	</div>

	<form action="/project/create" class = "model__form">
		<button>Create Project</button>
	</form>

<jsp:include page="../include/_footer.jsp"/>