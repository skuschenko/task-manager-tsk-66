package com.tsc.skuschenko.tm.controller;

import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.repository.ProjectRepository;
import com.tsc.skuschenko.tm.repository.TaskRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collection;
import java.util.UUID;

@Controller
public class TaskEditController {

    @Autowired
    @NotNull
    private ProjectRepository projectRepository;

    @Autowired
    @NotNull
    private TaskRepository taskRepository;

    @GetMapping("/task/create")
    @NotNull
    public String create() {
        @NotNull final Task task =
                new Task("Task" + UUID.randomUUID().toString());
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    @NotNull
    public String delete(@PathVariable("id") @NotNull final String id) {
        taskRepository.removeById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    @NotNull
    public String edit(
            @ModelAttribute("task") @NotNull final Task task,
            @NotNull final BindingResult result
    ) {
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
            task.setProjectId(null);
        }
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    @NotNull
    public ModelAndView edit(@PathVariable("id") @NotNull final String id) {
        @NotNull final Task task = taskRepository.findById(id);
        return new ModelAndView(
                "task-edit", "task",
                task
        );
    }

    @ModelAttribute("projects")
    @NotNull
    public Collection<Project> getProjects() {
        return projectRepository.findAll();
    }

    @ModelAttribute("statuses")
    @NotNull
    public Status[] getStatuses() {
        return Status.values();
    }

}
