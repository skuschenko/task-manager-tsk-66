package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ICommandRepository {

    void add(@NotNull AbstractListener abstractCommand);

    @NotNull
    Collection<AbstractListener> getArgs();

    @NotNull
    Collection<AbstractListener> getArguments();

    @NotNull
    Collection<String> getCommandArgs();

    @NotNull
    AbstractListener getCommandByArg(@NotNull String name);

    @Nullable
    AbstractListener getCommandByName(@NotNull String name);

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<AbstractListener> getCommands();

}